cmake_minimum_required(VERSION 3.8)
project(QubeCinemasChallenge)

set(CMAKE_CXX_STANDARD 17)

enable_testing()

set(SRC
    "include/utils.h"

    "include/IDelivery.h"
    "include/Delivery.h"
    "src/Delivery.cpp"

    "include/IPartner.h"
    "include/Partner.h"
    "src/Partner.cpp"

    "include/CSVRow.h"
    "src/CSVRow.cpp"

    "include/CSVReader.h"
    "src/CSVReader.cpp"

    "include/CSVWriter.h"
    "src/CSVWriter.cpp"

    "include/Solution.h"
    "src/Solution.cpp"

    "include/TheaterPartnerCosts.h"
    "src/TheaterPartnerCosts.cpp"

    "include/MinCostPartnerAlgorithm.h"
    "src/MinCostPartnerAlgorithm.cpp"

    "include/DeliveryCapacityAlgorithm.h"
    "src/DeliveryCapacityAlgorithm.cpp"

    "include/DeliveryAdapter.h"
    "src/DeliveryAdapter.cpp"

    "include/PartnerAdapter.h"
    "src/PartnerAdapter.cpp"
)

add_executable(${PROJECT_NAME} "src/main.cpp" ${SRC})
target_include_directories(${PROJECT_NAME} PUBLIC "include")

add_executable("${PROJECT_NAME}Tests" ${SRC}
    "test/MinCostPartnerAlgorithmTest.cpp"
)
target_include_directories("${PROJECT_NAME}Tests" PUBLIC "include")
add_test(Tests "${PROJECT_NAME}Tests")
