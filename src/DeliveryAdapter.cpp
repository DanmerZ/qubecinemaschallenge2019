#include "DeliveryAdapter.h"

DeliveryPtr DeliveryAdapter::create(const DeliveryStruct &deliveryStruct) const
{
    return std::make_shared<Delivery>(deliveryStruct.deliveryID,
                                      deliveryStruct.contentSize, deliveryStruct.theaterID);
}

Deliveries DeliveryAdapter::create(const DeliveryStructs &deliveryStructs) const
{
    Deliveries deliveries;
    for (const auto& deliveryStruct : deliveryStructs)
    {
        deliveries.insert({ deliveryStruct.deliveryID,
            create(deliveryStruct) });
    }

    return deliveries;
}
