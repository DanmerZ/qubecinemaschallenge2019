#include "CSVRow.h"

const std::string WHITESPACE = " \n\r\t\f\v";

std::string ltrim(const std::string& s)
{
    size_t start = s.find_first_not_of(WHITESPACE);
    return (start == std::string::npos) ? "" : s.substr(start);
}

std::string rtrim(const std::string& s)
{
    size_t end = s.find_last_not_of(WHITESPACE);
    return (end == std::string::npos) ? "" : s.substr(0, end + 1);
}

std::string trim(const std::string& s)
{
    return rtrim(ltrim(s));
}

CSVRow::CSVRow(std::istream &stream)
{
    std::string line;
    std::getline(stream, line);

    std::stringstream lineStream(line);

    std::string cell;

    while (std::getline(lineStream, cell, ','))
    {
        cells_.push_back(trim(cell));
    }
}

CSVRow::Cells CSVRow::getCells() const
{
    return cells_;
}
