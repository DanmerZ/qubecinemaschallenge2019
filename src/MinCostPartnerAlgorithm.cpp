#include "MinCostPartnerAlgorithm.h"

UInt MinCostPartnerAlgorithm::calculateCost(UInt contentSize,
                                            UInt minCost, UInt costPerGB)
{
    UInt cost = costPerGB * contentSize;
    if (cost < minCost)
        cost = minCost;
    return cost;
}

bool MinCostPartnerAlgorithm::findMinCostPartner(
        const TheaterPartnerCosts::TheaterCostInfoList& costInfoList,
        UInt contentSize, MinCostPartner &minCostPartner)
{
    bool foundMinCostPartner = false;

    for (const auto& costInfo : costInfoList)
    {
        if (contentSize > costInfo.minSizeSlab_ && contentSize <= costInfo.maxSizeSlab_)
        {
            auto cost = calculateCost(contentSize, costInfo.minCost_, costInfo.costPerGB_);
            if (cost < minCostPartner.cost_)
            {
                minCostPartner.cost_ = cost;
                minCostPartner.partner_ = costInfo.partner_;
                foundMinCostPartner = true;
            }
        }
    }

    return foundMinCostPartner;
}
