#include "PartnerAdapter.h"

PartnerPtr PartnerAdapter::createPartner(
        const PartnerStruct &partnerStruct) const
{
    return std::make_shared<Partner>(partnerStruct.partnerID);
}
