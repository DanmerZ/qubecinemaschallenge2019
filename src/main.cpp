#include <iostream>

#include "Solution.h"

int main(int argc, char * argv[])
{
    std::cout << "Enter path to the directory with CSV files (. by default): " << std::endl;
    std::string path;
    std::cin >> path;

    if (path.empty())
        path = ".";

    Solution solution(path);

    std::cout << "Select problem (1 or 2): ";

    int problem;
    std::cin >> problem;

    if (problem == 1)
        solution.problem1();
    else if (problem == 2)
        solution.problem2();

    return 0;
}
