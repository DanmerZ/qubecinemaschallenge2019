#include "TheaterPartnerCosts.h"

void TheaterPartnerCosts::addCostInfo(const TheaterID &theaterID,
                                      const PartnerPtr &partner,
                                      UInt minCost, UInt minSizeSlab,
                                      UInt maxSizeSlab, UInt costPerGB)
{
    theaterCostInfo_[theaterID].push_back(
    {
        partner, minCost, minSizeSlab, maxSizeSlab, costPerGB
                });
}

TheaterPartnerCosts::TheaterCostInfoList
TheaterPartnerCosts::getTheaterCostListByTheaterID(const TheaterID &theaterID)
{
    if (theaterCostInfo_.count(theaterID) > 0)
        return theaterCostInfo_.at(theaterID);

    return {};
}

TheaterPartnerCosts::TheaterCosts TheaterPartnerCosts::getTheaterCosts() const
{
    return theaterCostInfo_;
}

