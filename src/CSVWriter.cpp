#include "CSVWriter.h"

#include <fstream>

void CSVWriter::printOutput(const CSVWriter::Output &output,
                            const std::string& outFile)
{
    std::ofstream of(outFile, std::ios::out);

    for (const auto& out : output)
    {
        of << out.deliveryID << "," << (out.success ? "true" : "false") << ","
           << (out.partnerID.empty() ? "\"\"" : out.partnerID) << ",";

        if (out.cost != 0u)
        {
            of << out.cost;
        }
        else
        {
            of << "\"\"";
        }

        of << std::endl;
    }
}
