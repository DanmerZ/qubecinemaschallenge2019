#include "Delivery.h"

Delivery::Delivery(const std::string &deliveryID, UInt contentSize, TheaterID theaterID)
    : deliveryID_(deliveryID),
      contentSize_(contentSize),
      theaterID_(theaterID)
{ }

std::string Delivery::getDeliveryID() const
{
    return deliveryID_;
}

UInt Delivery::getContentSize() const
{
    return contentSize_;
}

TheaterID Delivery::getTheaterID() const
{
    return theaterID_;
}

