#include "CSVReader.h"

UInt CSVReader::toUInt(const std::string& s) const
{
    return static_cast<UInt>(std::stoi(s));
}

std::pair<UInt, UInt> CSVReader::splitSlab(const std::string &slab) const
{
    std::stringstream ss(slab);
    std::string minSlab, maxSlab;
    std::getline(ss, minSlab, '-');
    std::getline(ss, maxSlab, '-');

    return { toUInt(minSlab), toUInt(maxSlab) };
}

DeliveryStructs CSVReader::readDeliveryStructs(const std::string &input) const
{
    std::ifstream fs(input, std::ios::in);

    DeliveryStructs deliveries;
    std::string s;
    while (std::getline(fs, s, '\n'))
    {
        std::stringstream sstream(s);
        CSVRow row(sstream);
        const auto& cells = row.getCells();

        if (cells.empty())
            continue;

        deliveries.push_back(
            { cells[0], cells[2], toUInt(cells[1]) });
    }

    return deliveries;
}

PartnerStructs CSVReader::readPartnerStructs(const std::string &partners) const
{
    std::ifstream fs(partners, std::ios::in);

    PartnerStructs partnerStructs;
    std::string s;
    std::getline(fs, s, '\n'); // skip first line

    while (std::getline(fs, s, '\n'))
    {
        std::stringstream sstream(s);
        CSVRow row(sstream);
        const auto& cells = row.getCells();

        if (cells.empty())
            continue;

        auto [minSlab, maxSlab] = splitSlab(cells[1]);

        partnerStructs.push_back(
            { cells[4], cells[0], toUInt(cells[2]), minSlab, maxSlab, toUInt(cells[3]) });
    }

    return partnerStructs;
}

PartnerCapacitiesStructs CSVReader::readCapacities(const std::string capacities) const
{
    std::ifstream fs(capacities, std::ios::in);

    PartnerCapacitiesStructs capStructs;
    std::string s;
    std::getline(fs, s, '\n'); // skip first line
    while (std::getline(fs, s, '\n'))
    {
        std::stringstream sstream(s);
        CSVRow row(sstream);
        const auto& cells = row.getCells();

        if (cells.empty())
            continue;

        capStructs.push_back(
            { cells[0], toUInt(cells[1]) });
    }

    return capStructs;
}
