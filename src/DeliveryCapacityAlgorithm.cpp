#include "DeliveryCapacityAlgorithm.h"

#include <algorithm>

#include "MinCostPartnerAlgorithm.h"

bool operator<(const DeliveryPartnerCost &lhs, const DeliveryPartnerCost &rhs)
{
    return lhs.cost < rhs.cost;
}

DeliveryPartnerCostMap DeliveryCapacityAlgorithm::findOptimalDeliveryPartnersWithCapacity(
        const TheaterPartnerCosts::TheaterCosts &theaterCostInfo,
        const Deliveries &deliveries, const Partners &partners)
{
    auto deliveryPartners = findAllPossibleDeliveryPartners(
                theaterCostInfo, deliveries);

    DeliveryPartnerCostMap optimalDeliveries;

    bool capacityExceed = true;
    while (capacityExceed)
    {
        std::map<PartnerID, UInt> capacityBalance;
        optimalDeliveries.clear();

        for (const auto& [deliveryID, costSet] : deliveryPartners)
        {
            for (const auto& costItem : costSet)
            {
                const auto& partnerID = costItem.partner->getPartnerID();
                bool isPartnerCounted = (capacityBalance.count(partnerID) > 0);
                if (!isPartnerCounted ||
                        costItem.partner->getCapacity() > capacityBalance.at(partnerID))
                {
                    optimalDeliveries.insert({ deliveryID, costItem });
                    isPartnerCounted ? capacityBalance[partnerID] += costItem.delivery->getContentSize() :
                            capacityBalance[partnerID] = costItem.delivery->getContentSize();
                    break;
                }
            }
        }

        capacityExceed = false;
        for (const auto& [partnerID, cap] : capacityBalance)
        {
            if (cap > partners.at(partnerID)->getCapacity())
            {
                capacityExceed = true;

                for (auto& [deliveryID, costSet] : deliveryPartners)
                {
                    auto it = std::find_if(costSet.begin(), costSet.end(),
                                 [&partnerID](auto& item) { return item.partner->getPartnerID() == partnerID; });
                    if (it != costSet.end())
                    {
                        costSet.erase(it);
                        break;
                    }
                }
            }
        }
    }

    return optimalDeliveries;
}

DeliveryCapacityAlgorithm::DeliveryPartners
DeliveryCapacityAlgorithm::findAllPossibleDeliveryPartners(
        const TheaterPartnerCosts::TheaterCosts &theaterCostInfo,
        const Deliveries &deliveries) const
{
    DeliveryPartners deliveryPartners;

    for (const auto& deliveryPair : deliveries)
    {
        const auto& deliveryID = deliveryPair.first;
        const auto delivery = deliveryPair.second;

        const auto& theaterID = delivery->getTheaterID();
        const auto contentSize = delivery->getContentSize();

        if (theaterCostInfo.count(theaterID) == 0)
            continue;

        for (const auto& costInfo : theaterCostInfo.at(theaterID))
        {
            const auto& partner = costInfo.partner_;
            if (contentSize > costInfo.minSizeSlab_ && contentSize <= costInfo.maxSizeSlab_)
            {
                auto cost = MinCostPartnerAlgorithm::calculateCost(contentSize, costInfo.minCost_, costInfo.costPerGB_);
                deliveryPartners[deliveryID].insert({delivery, partner, cost});
            }
        }
    }

    return deliveryPartners;
}
