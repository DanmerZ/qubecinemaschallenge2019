#include "Solution.h"

#include <algorithm>

#include "CSVReader.h"
#include "CSVWriter.h"
#include "Delivery.h"
#include "DeliveryAdapter.h"
#include "DeliveryCapacityAlgorithm.h"
#include "MinCostPartnerAlgorithm.h"
#include "Partner.h"
#include "PartnerAdapter.h"

Solution::Solution(const std::string& path)
{
    CSVReader csvReader;

    auto deliveryStructs = csvReader.readDeliveryStructs(path + "/input.csv");
    initDeliveries(deliveryStructs);

    auto capacityStructs = csvReader.readCapacities(path + "/capacities.csv");

    auto partnerStructs = csvReader.readPartnerStructs(path + "/partners.csv");
    initPartners(partnerStructs, capacityStructs);
}

void Solution::problem1()
{
    CSVWriter::Output output;

    for (const auto& [deliveryID, delivery] : deliveries_)
    {
        MinCostPartner minCostPartner;
        auto success = MinCostPartnerAlgorithm::findMinCostPartner(
            theaterPartnerCosts_.getTheaterCostListByTheaterID(
                        delivery->getTheaterID()),
                    delivery->getContentSize(), minCostPartner);
        if (success)
        {
            output.push_back({
                deliveryID, success, minCostPartner.partner_->getPartnerID(),
                minCostPartner.cost_ });
        }
        else
        {
            output.push_back({ deliveryID, success, "", 0 });
        }
    }

    CSVWriter csvWriter;
    csvWriter.printOutput(output, "output.csv");
}

void Solution::problem2()
{
    DeliveryCapacityAlgorithm capacityAlgo;
    auto optimalDeliveries = capacityAlgo.findOptimalDeliveryPartnersWithCapacity(
        theaterPartnerCosts_.getTheaterCosts(), deliveries_, partners_);

    CSVWriter::Output output;

    for (const auto& [deliveryID, _] : deliveries_)
    {
        auto optimalDeliveryIt = optimalDeliveries.find(deliveryID);

        if (optimalDeliveryIt != optimalDeliveries.end())
        {
            const auto& deliveryPartnerCost = optimalDeliveryIt->second;
            output.push_back({
                deliveryID, true, deliveryPartnerCost.partner->getPartnerID(),
                deliveryPartnerCost.cost});
        }
        else
        {
             output.push_back({ deliveryID, false, "", 0 });
        }
    }

    CSVWriter csvWriter;
    csvWriter.printOutput(output, "output.csv");
}

void Solution::initDeliveries(const DeliveryStructs &deliveryStructs)
{
    DeliveryAdapter deliveryAdapter;
    deliveries_ = deliveryAdapter.create(deliveryStructs);
}

void Solution::initPartners(const PartnerStructs &partnerStructs,
                            const PartnerCapacitiesStructs& partnerCapacities)
{
    PartnerAdapter partnerAdapter;
    for (const auto& partnerStruct : partnerStructs)
    {
        PartnerPtr partner = (partners_.count(partnerStruct.partnerID) > 0) ?
                    partners_.at(partnerStruct.partnerID) :
                    partnerAdapter.createPartner(partnerStruct);
        partners_.insert({partnerStruct.partnerID, partner});

        theaterPartnerCosts_.addCostInfo(partnerStruct.theaterID, partner,
            partnerStruct.minCost, partnerStruct.minSizeSlab,
            partnerStruct.maxSizeSlab, partnerStruct.costPerGB);
    }

    for (const auto& capacityStruct : partnerCapacities)
    {
        if (partners_.count(capacityStruct.partnerID) > 0)
        {
            partners_.at(capacityStruct.partnerID)->setCapacity(
                capacityStruct.capacity);
        }
    }
}

