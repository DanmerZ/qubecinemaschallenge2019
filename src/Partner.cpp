#include "Partner.h"

Partner::Partner(const std::string &partnerID)
    : partnerID_(partnerID)
{ }

std::string Partner::getPartnerID() const
{
    return partnerID_;
}

UInt Partner::getCapacity() const
{
    return capacity_;
}

void Partner::setCapacity(UInt capacity)
{
    capacity_ = capacity;
}

