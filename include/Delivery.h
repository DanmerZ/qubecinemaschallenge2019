#pragma once

#include "IDelivery.h"

class Delivery : public IDelivery
{
public:
    Delivery(const std::string& deliveryID, UInt contentSize, TheaterID theaterID);

    std::string getDeliveryID() const override;
    UInt getContentSize() const override;
    TheaterID getTheaterID() const override;

private:
    const std::string deliveryID_;
    UInt contentSize_;
    TheaterID theaterID_;
};
