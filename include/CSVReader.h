#include "CSVRow.h"

#include "IDelivery.h"
#include "IPartner.h"

class CSVReader
{
public:
    DeliveryStructs readDeliveryStructs(const std::string& input) const;
    PartnerStructs readPartnerStructs(const std::string& partners) const;
    PartnerCapacitiesStructs readCapacities(const std::string capacities) const;

private:
    UInt toUInt(const std::string& s) const;
    std::pair<UInt, UInt> splitSlab(const std::string& slab) const;

};
