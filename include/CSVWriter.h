#pragma once

#include "IDelivery.h"

class CSVWriter
{
public:
    struct Out
    {
        DeliveryID deliveryID;
        bool success;
        PartnerID partnerID;
        UInt cost;
    };

    using Output = std::vector<Out>;

    void printOutput(const Output& output, const std::string& outFile);
};
