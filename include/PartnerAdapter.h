#pragma once

#include "Partner.h"

class PartnerAdapter
{
public:
    PartnerPtr createPartner(const PartnerStruct& partnerStruct) const;
};
