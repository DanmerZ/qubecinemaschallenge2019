#pragma once

#include "utils.h"

class IDelivery
{
public:
    virtual ~IDelivery() = default;
    virtual std::string getDeliveryID() const = 0;
    virtual UInt getContentSize() const = 0;
    virtual TheaterID getTheaterID() const = 0;
};

using DeliveryPtr = std::shared_ptr<IDelivery>;
using Deliveries = std::map<DeliveryID, DeliveryPtr>;

struct DeliveryStruct
{
    std::string deliveryID;
    std::string theaterID;
    UInt contentSize;
};

using DeliveryStructs = std::vector<DeliveryStruct>;
