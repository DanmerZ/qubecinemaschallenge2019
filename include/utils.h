#pragma once

#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>

using UInt = unsigned int;

using TheaterID = std::string;
using PartnerID = std::string;
using DeliveryID = std::string;
