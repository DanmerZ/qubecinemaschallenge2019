#pragma once

#include "IPartner.h"

struct TheaterCostInfo
{
    PartnerPtr partner_;
    const UInt minCost_;
    const UInt minSizeSlab_;
    const UInt maxSizeSlab_;
    const UInt costPerGB_;
};

class TheaterPartnerCosts
{
public:
    using TheaterCostInfoList = std::vector<TheaterCostInfo>;
    using TheaterCosts = std::map<TheaterID, TheaterCostInfoList>;

    void addCostInfo(const TheaterID& theaterID,
            const PartnerPtr& partner, UInt minCost, UInt minSizeSlab,
            UInt maxSizeSlab, UInt costPerGB);

    TheaterCostInfoList getTheaterCostListByTheaterID(
        const TheaterID& theaterID);

    TheaterCosts getTheaterCosts() const;

private:
    TheaterCosts theaterCostInfo_;
};
