#pragma once

#include "IPartner.h"

class Partner : public IPartner
{
public:
    Partner(const std::string& partnerID);

    std::string getPartnerID() const override;

    UInt getCapacity() const override;

    void setCapacity(UInt capacity) override;


private:
    const std::string partnerID_;
    UInt capacity_ = std::numeric_limits<UInt>::max();
};
