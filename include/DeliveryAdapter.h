#pragma once

#include "Delivery.h"

class DeliveryAdapter
{
public:
    DeliveryPtr create(const DeliveryStruct& deliveryStruct) const;
    Deliveries create(const DeliveryStructs& deliveryStructs) const;
};
