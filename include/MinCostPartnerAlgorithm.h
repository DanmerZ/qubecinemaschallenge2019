#pragma once

#include "IPartner.h"
#include "TheaterPartnerCosts.h"

struct MinCostPartner
{
    PartnerPtr partner_;
    UInt cost_ = std::numeric_limits<UInt>::max();
};

class MinCostPartnerAlgorithm
{
public:
    static UInt calculateCost(UInt contentSize, UInt minCost, UInt costPerGB);
    static bool findMinCostPartner(
            const TheaterPartnerCosts::TheaterCostInfoList& costInfoList,
            UInt contentSize, MinCostPartner& minCostPartner);
};

