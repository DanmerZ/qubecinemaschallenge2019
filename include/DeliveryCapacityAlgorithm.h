#pragma once

#include "IDelivery.h"
#include "IPartner.h"
#include "TheaterPartnerCosts.h"

struct DeliveryPartnerCost
{
    DeliveryPtr delivery;
    PartnerPtr partner;
    UInt cost;
};

bool operator<(const DeliveryPartnerCost& lhs, const DeliveryPartnerCost& rhs);

using Deliveries = std::map<DeliveryID, DeliveryPtr>;
using Partners = std::map<PartnerID, PartnerPtr>;

using DeliveryPartnerCostMap = std::map<DeliveryID, DeliveryPartnerCost>;

class DeliveryCapacityAlgorithm
{
public:
    using DeliveryPartners = std::map<DeliveryID, std::set<DeliveryPartnerCost>>;

    DeliveryPartnerCostMap findOptimalDeliveryPartnersWithCapacity(
        const TheaterPartnerCosts::TheaterCosts& theaterCostInfo,
            const Deliveries& deliveries, const Partners& partners);

private:
    DeliveryPartners findAllPossibleDeliveryPartners(
            const TheaterPartnerCosts::TheaterCosts &theaterCostInfo,
            const Deliveries &deliveries) const;
};
