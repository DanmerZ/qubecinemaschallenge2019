#pragma once

#include <fstream>
#include <sstream>
#include <string>
#include <vector>

class CSVRow
{
public:
    using Cells = std::vector<std::string>;
    CSVRow(std::istream& stream);

    Cells getCells() const;

private:
    Cells cells_;
};
