#pragma once

#include "utils.h"

class IPartner
{
public:
    virtual ~IPartner() = default;
    virtual std::string getPartnerID() const = 0;

    virtual UInt getCapacity() const = 0;
    virtual void setCapacity(UInt capacity) = 0;
};

using PartnerPtr = std::shared_ptr<IPartner>;
using Partners = std::map<PartnerID, PartnerPtr>;

struct PartnerStruct
{
    std::string partnerID;
    std::string theaterID;
    UInt minCost;
    UInt minSizeSlab;
    UInt maxSizeSlab;
    UInt costPerGB;
};

using PartnerStructs = std::vector<PartnerStruct>;

struct PartnerCapacitiesStruct
{
    std::string partnerID;
    UInt capacity;
};

using PartnerCapacitiesStructs = std::vector<PartnerCapacitiesStruct>;
