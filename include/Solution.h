#pragma once

#include "utils.h"

#include "IPartner.h"
#include "IDelivery.h"
#include "TheaterPartnerCosts.h"

class Solution
{
public:
    Solution(const std::string& path);
    void problem1();
    void problem2();

private:
    void initDeliveries(const DeliveryStructs& deliveryStructs);
    void initPartners(const PartnerStructs& partnerStructs,
        const PartnerCapacitiesStructs& partnerCapacities);

    Deliveries deliveries_;
    Partners partners_;
    TheaterPartnerCosts theaterPartnerCosts_;
};
