#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "MinCostPartnerAlgorithm.h"
#include "Delivery.h"
#include "DeliveryAdapter.h"
#include "Partner.h"
#include "PartnerAdapter.h"
#include "TheaterPartnerCosts.h"

TEST_CASE("calculateCost()", "MinCostPartnerAlgorithm")
{
    auto actualCost = MinCostPartnerAlgorithm::calculateCost(1, 2, 3);
    REQUIRE(actualCost == 3);
}

DeliveryStructs deliveryStructs1 =
{
    {"D1", "T1", 100},
    {"D2", "T1", 300},
    {"D3", "T1", 350}
};

DeliveryStructs deliveryStructs2 =
{
    {"D1", "T1", 70},
    {"D2", "T1", 300}
};

DeliveryStructs deliveryStructs3 =
{
    {"D1", "T3", 70},
    {"D2", "T1", 300}
};


PartnerStructs partnerStructs1 =
{
    {"P1", "T1", 2000u, 0u,   200u,  20u},
    {"P1", "T1", 3000u, 200u, 400u,  15u},
    {"P1", "T3", 4000u, 100u, 200u,  30u},
    {"P1", "T3", 5000u, 200u, 400u,  25u},
    {"P1", "T5", 2000u, 100u, 2000u, 30u},
    {"P2", "T1", 1500u, 0u,   400u,  25u}
};

PartnerCapacitiesStructs partnerCapacities1 =
{
    {"P1", 500},
    {"P2", 300}
};

struct MinCostPartnerAlgorithmFixture
{
    MinCostPartnerAlgorithmFixture(const DeliveryStructs& deliveryStructs)
    {
        deliveries_ = DeliveryAdapter().create(deliveryStructs);

        for (const auto& partnerStruct : partnerStructs1)
        {
            PartnerPtr partner = (partners_.count(partnerStruct.partnerID) > 0) ?
                        partners_.at(partnerStruct.partnerID) :
                        PartnerAdapter().createPartner(partnerStruct);
            partners_.insert({partnerStruct.partnerID, partner});

            theaterPartnerCosts_.addCostInfo(partnerStruct.theaterID, partner,
                partnerStruct.minCost, partnerStruct.minSizeSlab,
                partnerStruct.maxSizeSlab, partnerStruct.costPerGB);
        }
    }

    Deliveries deliveries_;
    Partners partners_;
    TheaterPartnerCosts theaterPartnerCosts_;
};

MinCostPartnerAlgorithmFixture fixture1(deliveryStructs1);
MinCostPartnerAlgorithmFixture fixture2(deliveryStructs2);
MinCostPartnerAlgorithmFixture fixture3(deliveryStructs3);

TEST_CASE("Sample scenario 1", "MinCostPartnerAlgorithm")
{
    SECTION("D1")
    {
        MinCostPartner minCostPartner;
        bool success = MinCostPartnerAlgorithm::findMinCostPartner(
            fixture1.theaterPartnerCosts_.getTheaterCostListByTheaterID("T1"),
            100, minCostPartner);

        REQUIRE(success);
        REQUIRE(minCostPartner.cost_ == 2000);
        REQUIRE(minCostPartner.partner_->getPartnerID() == "P1");
    }

    SECTION("D2")
    {
        MinCostPartner minCostPartner;
        bool success = MinCostPartnerAlgorithm::findMinCostPartner(
            fixture1.theaterPartnerCosts_.getTheaterCostListByTheaterID("T1"),
            300, minCostPartner);

        REQUIRE(success);
        REQUIRE(minCostPartner.cost_ == 4500);
        REQUIRE(minCostPartner.partner_->getPartnerID() == "P1");
    }

    SECTION("D3")
    {
        MinCostPartner minCostPartner;
        bool success = MinCostPartnerAlgorithm::findMinCostPartner(
            fixture1.theaterPartnerCosts_.getTheaterCostListByTheaterID("T1"),
            350, minCostPartner);

        REQUIRE(success);
        REQUIRE(minCostPartner.cost_ == 5250);
        REQUIRE(minCostPartner.partner_->getPartnerID() == "P1");
    }
}

TEST_CASE("Sample scenario 2", "MinCostPartnerAlgorithm")
{
    SECTION("D1")
    {
        MinCostPartner minCostPartner;
        bool success = MinCostPartnerAlgorithm::findMinCostPartner(
            fixture2.theaterPartnerCosts_.getTheaterCostListByTheaterID("T1"),
            70, minCostPartner);

        REQUIRE(success);
        REQUIRE(minCostPartner.cost_ == 1750);
        REQUIRE(minCostPartner.partner_->getPartnerID() == "P2");
    }

    SECTION("D2")
    {
        MinCostPartner minCostPartner;
        bool success = MinCostPartnerAlgorithm::findMinCostPartner(
            fixture2.theaterPartnerCosts_.getTheaterCostListByTheaterID("T1"),
            300, minCostPartner);

        REQUIRE(success);
        REQUIRE(minCostPartner.cost_ == 4500);
        REQUIRE(minCostPartner.partner_->getPartnerID() == "P1");
    }
}

TEST_CASE("Sample scenario 3", "MinCostPartnerAlgorithm")
{
    SECTION("D1")
    {
        MinCostPartner minCostPartner;
        bool success = MinCostPartnerAlgorithm::findMinCostPartner(
            fixture3.theaterPartnerCosts_.getTheaterCostListByTheaterID("T3"),
            70, minCostPartner);

        REQUIRE(!success);
    }

    SECTION("D2")
    {
        MinCostPartner minCostPartner;
        bool success = MinCostPartnerAlgorithm::findMinCostPartner(
            fixture3.theaterPartnerCosts_.getTheaterCostListByTheaterID("T1"),
            300, minCostPartner);

        REQUIRE(success);
        REQUIRE(minCostPartner.cost_ == 4500);
        REQUIRE(minCostPartner.partner_->getPartnerID() == "P1");
    }
}
